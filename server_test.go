package main

import (
	"sync"
	"testing"
)

func TestFunctionality(t *testing.T) {
	a := &admin{
		locks: make(map[string]*user),
		subs:  make(map[uid]subscription),
	}

	req := make(chan request)

	srv := &server{
		admin: a,
		req:   req,
	}

	go srv.listen()

	u := &user{
		ID:        "user 1",
		Watchlist: []string{"ticket1"},

		resp:    make(chan error),
		reports: make(chan map[string]uid),
		req:     req,
	}

	u2 := &user{
		ID:        "user 2",
		Watchlist: []string{},

		resp:    make(chan error),
		reports: make(chan map[string]uid),
		req:     req,
	}

	u.indexWatchlist()
	u2.lock("ticket1")
	m := u.subscribe()

	uid, ok := m["ticket1"]

	if !ok {
		t.Fatal("ticket not locked")
	}

	if uid != u2.ID {
		t.Fatalf("%s != %s", u2.ID, uid)
	}
}

func TestUserSubscribeNotify(t *testing.T) {
	a := &admin{
		locks: make(map[string]*user),
		subs:  make(map[uid]subscription),
	}

	req := make(chan request)

	srv := &server{
		admin: a,
		req:   req,
	}

	go srv.listen()

	u := &user{
		ID:        "user 1",
		Watchlist: []string{"ticket"},

		resp:    make(chan error),
		reports: make(chan map[string]uid),
		req:     req,
	}

	u.indexWatchlist()
	u.subscribe()

	wg := sync.WaitGroup{}
	wg.Add(1)

	go u.listen(func(report map[string]uid) {
		if uid, ok := report["ticket"]; !ok {
			t.Fatal("ticket not on report")

			if uid != "user 1" {
				t.Fatalf("wrong user: %s", uid)
			}
		}

		wg.Done()
	})

	u.lock("ticket")
	wg.Wait()
}
