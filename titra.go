package main

func main() {
	admin := &admin{
		locks: make(map[string]*user),
		subs:  make(map[uid]subscription),
	}

	server := &server{
		admin: admin,
		req:   make(chan request),
	}

	go server.listen()

	ws := &webserver{
		server: server,
	}

	ws.init()
}
