package main

import (
	"net/http"

	"log"

	"github.com/gorilla/websocket"
)

type webserver struct {
	server *server
}

type wsRequest struct {
	ReqType reqType `json:"reqType"`
	Ticket  string  `json:"ticket"`
}

func (srv *webserver) init() {
	var upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	http.HandleFunc("/websocket", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")

		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
			return
		}

		srv.handleClient(conn)
	})

	if err := http.ListenAndServe(":4500", nil); err != nil {
		panic(err)
	}
}

func (srv *webserver) readUser(conn *websocket.Conn) (*user, error) {
	u := new(user)
	err := conn.ReadJSON(u)
	return u, err
}

func (srv *webserver) handleClient(conn *websocket.Conn) {
	u, err := srv.readUser(conn)
	if err != nil {
		log.Fatalf("error handling user: %v", err)
	}

	u.reports = make(chan map[string]uid)
	u.resp = make(chan error)
	u.indexWatchlist()
	u.req = srv.server.req

	go u.listen(func(report map[string]uid) {
		if err := conn.WriteJSON(report); err != nil {
			log.Println(err)
		}
	})

	for {
		req := new(wsRequest)
		if err := conn.ReadJSON(req); err != nil {
			log.Println(err)
			break
		}

		switch req.ReqType {
		case subscribe:
			report := u.subscribe()

			if err := conn.WriteJSON(report); err != nil {
				log.Println(err)
			}

		case unsubscribe:
			u.unsubscribe()
		case lock:
			u.lock(req.Ticket)
		case unlock:
			u.unlock(req.Ticket)
		}
	}

	u.unsubscribe()
}
