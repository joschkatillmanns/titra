package main

import "log"

type request struct {
	u      *user
	t      reqType
	ticket string
}

type reqType string

const (
	subscribe   reqType = "subscribe"
	unsubscribe reqType = "unsubscribe"
	lock        reqType = "lock"
	unlock      reqType = "unlock"
)

type server struct {
	admin *admin
	req   chan request
}

func (s *server) listen() {
	for r := range s.req {
		switch r.t {
		case subscribe:
			r.u.reports <- s.admin.subscribe(r.u)
		case unsubscribe:
			s.admin.unsubscribe(r.u)
			r.u.resp <- nil
		case lock:
			err := s.admin.lock(r.u, r.ticket)
			if err != nil {
				log.Fatalf("lock: %v", err)
			}

			r.u.resp <- err

		case unlock:
			s.admin.unlock(r.u, r.ticket)
			r.u.resp <- nil
		}
	}
}
