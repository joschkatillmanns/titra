package main

type uid string

type admin struct {
	locks map[string]*user
	subs  map[uid]subscription
}

type subscription struct {
	watchlist map[string]struct{}
	resp      chan map[string]uid
}

func (a *admin) subscribe(u *user) map[string]uid {
	sub := subscription{
		watchlist: u.watchlist,
		resp:      u.reports,
	}

	a.subs[u.ID] = sub
	return a.report(u.watchlist)
}

func (a *admin) report(watchlist map[string]struct{}) map[string]uid {
	report := make(map[string]uid)

	for e := range watchlist {
		if _, ok := a.locks[e]; !ok {
			continue
		}

		report[e] = a.locks[e].ID
	}

	return report
}

func (a *admin) unsubscribe(u *user) {
	delete(a.subs, u.ID)
}

func (a *admin) lock(u *user, ticket string) error {
	// if _, ok := a.locks[ticket]; ok {
	// 	return fmt.Errorf("already locked")
	// }

	// guarantee single lock for each user
	for t, lockingUser := range a.locks {
		if lockingUser.ID != u.ID {
			continue
		}

		a.unlock(u, t)
	}

	a.locks[ticket] = u

	a.notify(ticket)
	return nil
}

func (a *admin) unlock(u *user, ticket string) {
	delete(a.locks, ticket)
}

func (a *admin) notify(ticket string) {
	for _, e := range a.subs {
		if _, ok := e.watchlist[ticket]; !ok {
			continue
		}

		e.resp <- a.report(e.watchlist)
	}
}
