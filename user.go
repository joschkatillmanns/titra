package main

type user struct {
	ID        uid      `json:"id"`
	Watchlist []string `json:"watchlist"`

	watchlist map[string]struct{}
	resp      chan error
	reports   chan map[string]uid
	req       chan request
}

func (u *user) subscribe() map[string]uid {
	u.req <- request{
		u: u,
		t: subscribe,
	}

	return <-u.reports
}

func (u *user) unsubscribe() {
	u.req <- request{
		u: u,
		t: unsubscribe,
	}

	<-u.resp
}

func (u *user) lock(ticket string) {
	u.req <- request{
		u:      u,
		t:      lock,
		ticket: ticket,
	}

	<-u.resp
}

func (u *user) unlock(ticket string) {
	u.req <- request{
		u:      u,
		t:      unlock,
		ticket: ticket,
	}

	<-u.resp
}

func (u *user) indexWatchlist() {
	u.watchlist = make(map[string]struct{})

	for _, e := range u.Watchlist {
		u.watchlist[e] = struct{}{}
	}
}

func (u *user) listen(fn func(report map[string]uid)) {
	for r := range u.reports {
		fn(r)
	}
}
