package main

import "testing"

func TestUserHasSingleLock(t *testing.T) {
	a := &admin{
		locks: make(map[string]*user),
		subs:  make(map[uid]subscription),
	}

	u := &user{
		watchlist: make(map[string]struct{}),
		ID:        "user1",
	}

	a.subscribe(u)
	if _, ok := a.subs[u.ID]; !ok {
		t.Fatalf("user not subscribed: %v", u)
	}

	_ = a.lock(u, "ticket1")
	_ = a.lock(u, "ticket2")

	if len(a.locks) != 1 {
		t.Fatalf("more than 1 lock")
	}

	if _, ok := a.locks["ticket2"]; !ok {
		t.Fatalf("ticket 2 is not locked")
	}
}

func TestSubscribe(t *testing.T) {
	a := &admin{
		locks: make(map[string]*user),
		subs:  make(map[uid]subscription),
	}

	watchlist := map[string]struct{}{
		"ticket1": struct{}{},
	}

	u := &user{
		watchlist: watchlist,
		ID:        "user1",
	}

	a.subscribe(u)
	if _, ok := a.subs[u.ID]; !ok {
		t.Fatalf("user not subscribed: %v", u)
	}
}

func TestReport(t *testing.T) {
	locks := map[string]*user{
		"ticket1": &user{ID: "user2"},
		"ticket2": &user{ID: "user3"},
		"ticket3": &user{ID: "user4"},
	}

	a := &admin{
		locks: locks,
		subs:  make(map[uid]subscription),
	}

	watchlist := map[string]struct{}{
		"ticket1":   struct{}{},
		"ticket2":   struct{}{},
		"ticket100": struct{}{},
	}

	u := &user{
		watchlist: watchlist,
		ID:        "user1",
	}

	report := a.subscribe(u)
	if _, ok := report["ticket1"]; !ok {
		t.Fatalf("ticket 1 missing from report")
	}

	if _, ok := report["ticket2"]; !ok {
		t.Fatalf("ticket 2 missing from report")
	}

	if _, ok := report["ticket100"]; ok {
		t.Fatalf("ticket 100 shouldn't be on report")
	}
}
